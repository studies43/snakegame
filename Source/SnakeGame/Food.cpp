// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "iostream"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

float timer;
// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	timer = 0;
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	timer += DeltaTime;
	if (timer >= 5) {
		std::cout << "yes";
		FVector NewLocation(rand()%720 - 360, rand() % 720 - 360, 0);
		FTransform NewTransform = FTransform(NewLocation);
		GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
		timer = 0;
	}
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) 
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) 
		{
			Snake->AddSnakeElement();
		}
	}
}
